#!/bin/bash
root_dir=$(pwd)
workspace=${2:-stage}
source .env.sh
case $1 in 
    terraform)
        cd ${root_dir}/terraform/preinstall
        terraform workspace select ${workspace}
        terraform init 
        if [[ $? != 0 ]]
        then
            terraform init --backend-config="access_key=${TF_VAR_SA_ID_KEY}"  --backend-config="secret_key=${TF_VAR_SA_SECRET_KEY}" -upgrade
        fi
        terraform apply --auto-approve
        cd ${root_dir}/terraform
        terraform workspace select ${workspace}
        terraform init 
        if [[ $? != 0 ]]
        then
            terraform init --backend-config="access_key=${TF_VAR_SA_ID_KEY}"  --backend-config="secret_key=${TF_VAR_SA_SECRET_KEY}" -upgarde
        fi
        terraform apply --auto-approve
        ;;
    ansible)
        cd ${root_dir}/ansible
        for host in $(cat ${root_dir}/hosts)
        do
            while [ -z $(timeout 2s nc $host 22 | grep "SSH") ]
            do
                echo "Waiting ssh starting on host - $host"
                sleep 2
            done
            echo -e "Host $host - OK!"
        done
        ansible-playbook -i inventory/inventory.ini kubespray/cluster.yml --become 
        echo "Installing nfs provisioner"
        helm repo add stable https://charts.helm.sh/stable && helm repo update
        kubectl create ns mounts
        helm install nfs-server stable/nfs-server-provisioner -n mounts 
        kubectl patch storageclass nfs -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
        ;;
    monitoring)
        cd ${root_dir}/monitoring
        kubectl create -f kube-prometheus/manifests/setup
        until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
        kubectl create -f kube-prometheus/manifests/
        ;;
    atlantis)
        cd ${root_dir}/monitoring
        kubectl create ns atlantis
        helm -n atlantis upgrade --install atlantis runatlantis/atlantis -f atlantis/values.yaml --set github.token=${ATLANTIS_TOKEN} --set github.secret=${ATLANTIS_WEBHOOK_SECRET}
        ;;
    all)
        $0 terraform
        $0 ansible
        $0 monitoring
        $0 atlantis
        ;;
    destroy)
        cd ${root_dir}/terraform
        terraform destroy --auto-approve
        sed -i.backup '/loadbalancer_apiserver:/{1!p;N;N;d;}' ${root_dirs}/ansible/inventory/group_vars/all/all.yml
        sed -i.backup 's/loadbalancer_apiserver:/# loadbalancer_apiserver:/' ${root_dir}/ansible/inventory/group_vars/all/all.yml
        sudo sed -i.backup '/dimpola.netology/d' /etc/hosts
        rm -rf ${root_dir}/ansible/inventory/{artifacts,credentials,id_rsa,inventory.ini} ${root_dir}/ansible/inventory/group_vars/all/all.yml.backup ${root_dir}/hosts
        ;;
    reload)
        $0 destroy
        $0 all
        ;;
    reset)
        cd ${root_dir}/ansible
        ansible-playbook -i inventory/inventory.ini kubespray/reset.yml --become --extra-vars "reset_confirmation=yes"
        ;;
    *)
        echo "There is no such command ${1}"
        exit -1
        ;;
esac