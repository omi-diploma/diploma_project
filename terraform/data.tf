data "yandex_iam_service_account" "worker" {
  name = "worker"
}

data "yandex_vpc_subnet" "subnets" {
  for_each  = { for subnet in var.SUBNETS : subnet.name => subnet }
  subnet_id = yandex_vpc_subnet.subnets[each.value.name].id
}

data "yandex_lb_network_load_balancer" "k8s_master_loadbalancer" {
  network_load_balancer_id = yandex_lb_network_load_balancer.k8s_master_lb.id
}