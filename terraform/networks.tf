resource "yandex_vpc_network" "vpc" {
  name = terraform.workspace
}

resource "yandex_vpc_subnet" "subnets" {
  for_each = { for subnet in var.SUBNETS : subnet.name => subnet }

  name           = "${terraform.workspace}-${each.value.name}-${each.value.zone}"
  v4_cidr_blocks = each.value.cidr_block

  network_id = yandex_vpc_network.vpc.id
  zone       = each.value.zone

  description = each.value.type
}

resource "yandex_lb_network_load_balancer" "k8s_master_lb" {
  name          = "k8s-master-loadbalancer"
  listener {
    name        = "k8s-master-listener"
    port        = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }
  attached_target_group {
    target_group_id = yandex_compute_instance_group.k8s_nodes["master-nodes"].load_balancer.0.target_group_id
    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
  depends_on = [
    yandex_compute_instance_group.k8s_nodes
  ]
}