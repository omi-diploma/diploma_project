variable "SA_ID_KEY" {}
variable "SA_SECRET_KEY" {}

variable "SUBNETS" {
  type = list(object({
    name       = string
    type       = string
    zone       = string
    cidr_block = list(string)
  }))

  default = [
    {
      name       = "private"
      type       = "private"
      zone       = "ru-central1-a"
      cidr_block = ["192.168.20.0/24"]
    },
    {
      name       = "public"
      type       = "public"
      zone       = "ru-central1-b"
      cidr_block = ["192.168.10.0/24"]
    }
  ]
}

variable "K8S_NODES" {
  type = list(object({
    name        = string
    role        = string
    ami         = string
    count       = number
    subnet_type = string
    resources = object({
      cores          = number
      fraction       = number
      ram            = number
      boot_disk_size = number
    })
  }))
  validation {
    condition     = var.K8S_NODES[*].role != "master" && var.K8S_NODES[*].role != "worker"
    error_message = "Node role must be only master or worker!"
  }

  default = [
    {
      name        = "master-nodes"
      role        = "master"
      ami         = "fd88d14a6790do254kj7" #centos-7
      count       = 1
      subnet_type = "public"
      resources = {
        cores          = 4
        fraction       = 20
        ram            = 4
        boot_disk_size = 50
      }
    },
    {
      name        = "worker-nodes"
      role        = "worker"
      ami         = "fd88d14a6790do254kj7" #centos-7
      count       = 2
      subnet_type = "public"
      resources = {
        cores          = 2
        fraction       = 20
        ram            = 2
        boot_disk_size = 100
      }
    }
  ]
}