terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
        }
    }
    required_version = ">= 0.13"
}

provider "yandex" {
    service_account_key_file = file("key.json")
}

# Install bucket for remote-states
variable "SA_ID_KEY" {}
variable "SA_SECRET_KEY" {}

resource "yandex_storage_bucket" "bucket_for_backend" {
    access_key = var.SA_ID_KEY
    secret_key = var.SA_SECRET_KEY
    bucket     = "bucket-for-states"
} 