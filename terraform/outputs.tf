
resource "local_file" "ssh_private" {
  content         = local.ssh_keys.private_key
  filename        = "../ansible/inventory/id_rsa"
  file_permission = "0700"
  depends_on = [
    yandex_iam_service_account_key.instance_ssh_key
  ]
}

resource "local_file" "kubespray_inventory" {
  content = templatefile("templates/ansible-inventory.tftpl",
    {
      master_nodes = local.master_instance_data,
      ansible_nodes = concat(local.worker_instance_data, local.master_instance_data)
    }
  )
  filename = "../ansible/inventory/inventory.ini"
}

resource "local_file" "host_addresses_for_script" {
  content   =<<-EOT
    %{for node in concat(local.worker_instance_data, local.master_instance_data ) ~}
    ${~ node.network_interface[0].nat_ip_address }
    %{ endfor }
  EOT
  filename  = "../hosts"
}

resource "null_resource" "replace_api_server_address" {
  provisioner "local-exec" {
    command = <<-EOT
      $(sed -i.backup 's/# loadbalancer_apiserver:/loadbalancer_apiserver:\n  address: ${local.master_instance_data[0].network_interface[0].nat_ip_address}\n  port: 6443/' ../ansible/inventory/group_vars/all/all.yml)
    EOT
    interpreter = [
      "/bin/bash",
      "-c"
    ]
    when = create
  }
}

resource "null_resource" "add_load_balancer_to_hosts" {
  provisioner "local-exec" {
    command = "$(echo ${data.yandex_lb_network_load_balancer.k8s_master_loadbalancer.listener[*].external_address_spec[0].address} diploma.netology.com grafana.diploma.netology.com django.diploma.netology.com >> /etc/hosts)" 
    interpreter = [
      "/bin/bash",
      "-c"
    ]
    when = create
  }
}