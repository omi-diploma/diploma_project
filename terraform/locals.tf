locals {
  node_list = flatten([for node in var.K8S_NODES :
    [for iterator in range(node.count) :
      merge(node, { name = "${node.name}-${iterator}" })
    ]
  ])

  worker_instance_data = flatten([for group in yandex_compute_instance_group.k8s_nodes : group.instances[*] if group.description == "worker"])
  master_instance_data = flatten([for group in yandex_compute_instance_group.k8s_nodes : group.instances[*] if group.description != "worker"])

  ssh_keys = {
    private_key = sensitive(try(yandex_iam_service_account_key.instance_ssh_key[0].private_key, file("~/.ssh/id_rsa")))
    public_key  = try(yandex_iam_service_account_key.instance_ssh_key[0].public_key, file("~/.ssh/id_rsa.pub"))
  }
}