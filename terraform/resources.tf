resource "yandex_iam_service_account_key" "instance_ssh_key" {
  count              = terraform.workspace == "prod" ? 1 : 0
  service_account_id = data.yandex_iam_service_account.worker.id
  key_algorithm      = "RSA_2048"
}

resource "yandex_container_registry" "registry" {
  name = "container-registry"
}

resource "yandex_container_repository" "workspace_repo" {
  name = "${yandex_container_registry.registry.id}/${terraform.workspace}"
}

resource "yandex_compute_instance_group" "k8s_nodes" {
  for_each            = {for index,node in var.K8S_NODES : node.name => node }

  name                = "k8s-${each.value.role}"
  description         = each.value.role

  service_account_id  = data.yandex_iam_service_account.worker.id

  instance_template {

        resources {
          cores         = each.value.resources.cores  
          memory        = each.value.resources.ram
          core_fraction = each.value.resources.fraction
        }

        boot_disk{
          initialize_params {
              image_id = each.value.ami
              size     = each.value.resources.boot_disk_size
            }
        }
        network_interface {
          network_id = yandex_vpc_network.vpc.id
          subnet_ids = ["${data.yandex_vpc_subnet.subnets[each.value.subnet_type].id}"]
          nat        = each.value.subnet_type == "public" ? true : false
        }
        metadata = {
          ssh_keys = "centos:${local.ssh_keys.public_key}"
        }
  }
  load_balancer {
    target_group_name = "k8s-${each.value.role}"
  }
  allocation_policy {
    zones =["${data.yandex_vpc_subnet.subnets[each.value.subnet_type].zone}"]
  }
  scale_policy {
    fixed_scale {
      size = each.value.count
    }
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 1
  }
  depends_on = [
    yandex_vpc_network.vpc,
    yandex_vpc_subnet.subnets,
    data.yandex_vpc_subnet.subnets
  ]
}