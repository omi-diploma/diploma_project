terraform {
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "bucket-for-states"
    key      = "states/remote-state.tfstate"
    region   = "ru-central1"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = file("preinstall/key.json")
}